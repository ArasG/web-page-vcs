<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>News</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />

  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="news-page">
  <?php include('views/header.php'); ?>

  <main>
    <section class="section-heading flex-container">
      <h2 class="text">News</h2>
    </section>

    <section class="news-tiles flex-container container">
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/skyscaper_from_above.jpg" alt="foto of buoldings" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis reprehenderit aute irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
            occaecat cupidatat.
          </p>
          <a href="single-new.php" class="tile-footer">Read More</a>
        </div>
      </div>
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/office.jpg" alt="foto of office" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis reprehenderit aute irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
            occaecat cupidatat non proident sunt.
          </p>
          <a href="" class="tile-footer">Read More</a>
        </div>
      </div>
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/typing_laptop.jpg" alt="foto of laptop" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis reprehenderit aute irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
            occaecat cupidatat non proident sunt in.
          </p>
          <a href="" class="tile-footer">Read More</a>
        </div>
      </div>
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/money.jpg" alt="foto of money" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis aute reprehenderit irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint.
          </p>
          <a href="" class="tile-footer">Read More</a>
        </div>
      </div>
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/skyscaper_from_above2.jpg" alt="foto of buoldings" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis aute reprehenderit irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
            occaecat cupidatat non proident sunt.
          </p>
          <a href="" class="tile-footer">Read More</a>
        </div>
      </div>
      <div class="tile-of-news">
        <div class="wrapper">
          <img src="images/img/money_in_mug.jpg" alt="foto of mug with money" />
          <h5>Reprehenderit in voluptate velit esse cillum</h5>
          <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
          <p class="tile-body opensans-400-16 color-grey">
            Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
            cupidatat non proident sunt in culpa.
          </p>
          <a href="" class="tile-footer">Read More</a>
        </div>
      </div>
    </section>
    <?php include('views/footer.php'); ?>
  </main>
  <script src="js.js"></script>

</body>

</html>