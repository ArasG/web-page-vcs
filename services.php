<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Services</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="services">
  <?php include('views/header.php'); ?>


  <main>
    <section class="section-heading flex-container">
      <h2 class="text">Financial Analysis</h2>
    </section>
    <section class="services">
      <div class="container-backgroud">
        <div class="container">
          <div class="business-solutions">
            <div class="flex-container business-solutions-flex">
              <div class="left-tile tile">
                <img src="images/img/chart.jpg" alt="" />
              </div>
              <div class="right-tile tile">
                <div class="flex-container two-color-h3">
                  <h3>Business</h3>
                  <h3>Solutions</h3>
                </div>
                <p>
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Blanditiis ducimus sed libero vel excepturi, iusto dolorem
                  doloremque numquam impedit porro error sapiente aut,
                  tempore, non quam consequuntur! Voluptatibus nam rerum
                  eligendi similique?
                </p>
                <div class="flex-container tick-with-text">
                  <i class="fas fa-check"></i>
                  <p>
                    Ipsum dolor sit amet consectetur adipisicing elit. Sint
                    officiis dolor nisi! Eum quis dolorem
                  </p>
                </div>
                <div class="flex-container tick-with-text">
                  <i class="fas fa-check"></i>
                  <p>
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse.
                  </p>
                </div>
                <p>
                  Adipisicing elit. Blanditiis ducimus sed libero vel
                  excepturi, iusto dolorem doloremque numquam impedit porro
                  error sapiente aut, tempore, non quam consequuntur!
                </p>
              </div>
            </div>
          </div>
          <div class="flex-container two-color-h3 two-color-h3-centered">
            <h3>Our</h3>
            <h3>Pricing</h3>
          </div>
          <div class="flex-container pricing-tiles">
            <div class="pricing-tile">
              <p>Starter</p>
              <p>Free</p>
              <p>for up to 2 editors and 3 projects</p>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>3 projects</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>30-day version history</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Up to 2 editors</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Unlimited cloud storage</p>
              </div>
              <a href="#" class="btn btn-green btn-centered">Get Now</a>
            </div>
            <div class="pricing-tile highlighted">
              <p>Professional</p>
              <p>$9.99/mo</p>
              <p>for up to 2 editors and 3 projects</p>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>3 projects</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>30-day version history</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Up to 2 editors</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Unlimited cloud storage</p>
              </div>
              <div class="yellow-box">
                <span class="black-text">POPULAR</span>
              </div>
              <a href="#" class="btn btn-green btn-centered">Get Now</a>
            </div>
            <div class="pricing-tile">
              <p>Organization</p>
              <p>$19.99/mo</p>
              <p>for up to 2 editors and 3 projects</p>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>3 projects</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>30-day version history</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Up to 2 editors</p>
              </div>
              <div class="flex-container tick-and-text">
                <i class="fas fa-check-circle"></i>
                <p>Unlimited cloud storage</p>
              </div>
              <a href="#" class="btn btn-green btn-centered">Get Now</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="client-says">
      <div class="container">
        <div class="flex-container two-color-h3 two-color-h3-centered">
          <h3>Client</h3>
          <h3>Says</h3>
        </div>
        <div id="cardSlider" class="carousel mb-6 max-w-6xl mx-auto bg-gray-50 py-10">
          <div class="carousel__viewport px-12">
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="client-says-tile">
                <i class="fas fa-quote-right"></i>
                <div class="flex-container client-says-tile-container">
                  <p class="roboto-italic-27">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Aperiam nesciunt accusantium nulla inventore, enim
                    tempore!
                  </p>
                  <div class="flex-container tile-footer">
                    <img src="images/icons/Mask Group3.jpg" alt="clients photo" class="round-image" />
                    <div class="right-side">
                      <h4 class="lato-bold-21">Rudhi Sasmito</h4>
                      <p class="roboto-italic-16">ABC Company</p>
                    </div>
                  </div>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="client-says-tile">
                <i class="fas fa-quote-right"></i>
                <div class="flex-container client-says-tile-container">
                  <p class="roboto-italic-27">
                    Amet consectetur adipisicing elit. Aperiam nesciunt
                    accusantium nulla inventore, enim tempore!
                  </p>
                  <div class="flex-container tile-footer">
                    <img src="images/icons/Mask Group (1).png" alt="clients photo" />
                    <div class="right-side">
                      <h4 class="lato-bold-21">Sam</h4>
                      <p class="roboto-italic-16">Company BC</p>
                    </div>
                  </div>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="client-says-tile">
                <i class="fas fa-quote-right"></i>
                <div class="flex-container client-says-tile-container">
                  <p class="roboto-italic-27">
                    Pperiam nesciunt accusantium nulla inventore, enim
                    tempore!
                  </p>
                  <div class="flex-container tile-footer">
                    <img src="images/icons/Mask Group2.jpg" alt="clients photo" class="round-image" />
                    <div class="right-side">
                      <h4 class="lato-bold-21">John John</h4>
                      <p class="roboto-italic-16">HHH Company</p>
                    </div>
                  </div>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="client-says-tile">
                <i class="fas fa-quote-right"></i>
                <div class="flex-container client-says-tile-container">
                  <p class="roboto-italic-27">
                    Dolor sit, amet consectetur adipisicing elit. Aperiam
                    nesciunt accusantium nulla inventore, enim tempore!
                  </p>
                  <div class="flex-container tile-footer">
                    <img src="images/icons/Mask Group.png" alt="clients photo" />
                    <div class="right-side">
                      <h4 class="lato-bold-21">Andy A.</h4>
                      <p class="roboto-italic-16">Company BC</p>
                    </div>
                  </div>
                </div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
    <?php include('views/footer.php'); ?>

  </main>
  <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
  <script src="js.js"></script>

</body>

</html>