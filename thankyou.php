<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Contact</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="th">
  <?php include('views/header.php'); ?>

  <main class="thankyou">
    <section class="section-heading flex-container">
      <h2 class="text">Thank You!</h2>
      <h5>Your message has been successfully sent</h5>
      <h5>We will contact you soon</h5>
      <div class="flex-container">
        <a href="index.php" class="btn btn-green btn-centered">Back To Main</a>
      </div>
    </section>
  </main>
  <?php include('views/footer_2.php'); ?>

</body>

</html>