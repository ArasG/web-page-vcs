<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>About</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="about">
  <?php include('views/header.php'); ?>

  <main>
    <section class="section-heading flex-container">
      <h2 class="text">About</h2>
    </section>
    <section class="about-company">
      <div class="container flex-container">
        <div class="green-box">
          <p>
            Excepteur sint occaecat cupidatat non proident sunt in culpa qui
            officia.
          </p>
        </div>
        <div class="left-tile">
          <div class="background">
            <div class="green-box1"></div>
          </div>
        </div>
        <div class="right-tile">
          <div class="flex-container two-color-h3">
            <h3>Our</h3>
            <h3>Company</h3>
          </div>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Blanditiis ducimus sed libero vel excepturi, iusto dolorem
            doloremque numquam impedit porro error sapiente aut, tempore, non
            quam consequuntur! Voluptatibus nam rerum eligendi similique?
          </p>

          <div class="text">
            <p>
              Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
              cupidatat non proident, sunt in culpa qui officia deserunt
              mollit anim id est laborum.
            </p>
            <p>Rudhi Sasmito</p>
            <p class="roboto-italic-16">- Head Of Operations</p>
          </div>
        </div>
      </div>
    </section>
    <section class="about-team">
      <div class="container">
        <div class="flex-container two-color-h3">
          <h3>Our</h3>
          <h3>Team</h3>
        </div>
        <div id="cardSlider" class="carousel mb-6 max-w-6xl mx-auto bg-gray-50 py-10">
          <div class="carousel__viewport px-12">
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee1.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">John Doe</p>
                <p>Founder & CEO</p>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee3.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">Paddy O'Furniture</p>
                <p>Chief Finance Officer</p>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee2.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">Maureen Biologist</p>
                <p>Chief Marketing Officer</p>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee4.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">Peg Legge</p>
                <p>Chief Ligistics</p>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee5.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">Teri Dactyl</p>
                <p>HR Officer</p>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="team-tile">
                <img src="images/img/employee6.jpg" alt="employee" width="370" height="370" />
                <div class="background"></div>
                <p class="lato-bold-21">Olive Yew</p>
                <p>Finance Officer</p>
              </div>
            </figure>

          </div>
        </div>

      </div>
      <div class="container-backgroud">
        <div class="container">
          <div class="customers-footer flex-container dummy-logo">
            <img src="images/logos/dummy-logo-1b.png" alt="logo" />
            <img src="images/logos/dummy-logo-2b.png" alt="logo" />
            <img src="images/logos/dummy-logo-3b.png" alt="logo" />
            <img src="images/logos/dummy-logo-4b.png" alt="logo" />
          </div>
        </div>
      </div>
    </section>
    <!-- ////////////////.....FOOTER...../////////////// -->

    <?php include('views/footer.php'); ?>

  </main>
  <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
  <script src="js.js"></script>
</body>

</html>