<?php require __DIR__ . '../php/app.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Contact</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="css/styles.css" />

  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="contact">
  <?php include('views/header.php'); ?>

  <main class="contact">
    <section class="section-heading flex-container">
      <h2 class="text">Contact</h2>
    </section>
    <section class="section-body">
      <div class="container">
        <div class="map-wrapper">
          <img src="images/img/map.jpg" alt="map">
        </div>
        <!-- /////////////..... FORM CONTAINER .....//////////// -->
        <div class="contact-form-container flex-container container">
          <div class="tile left-side">
            <!-- <img src="images/img/phone.jpg" alt="phone"> -->
            <div class="contact-green-box flex-container">
              <div class=" tile left-side custom-flex">
                <i class="far fa-map"></i>
                <p class="header lato-bold-21">Address</p>
                <p class="body opensans-400-16 color-grey">99 S.t Jomblo Park
                  Pekanbaru 28292.
                  Indonesia</p>
                <div class="footer opensans-400-16 color-grey">
                  <p>Mon – Fri: <br> 8AM — 4PM
                  </p>
                  <p>Saturday: <br> 8AM — 4PM</p>
                </div>
              </div>
              <div class="tile right-side custom-flex">
                <i class="fas fa-phone-alt"></i>
                <p class="header lato-bold-21">Phone & Email</p>
                <div class="body opensans-400-16 color-grey">
                  <a href="tel:443330144501" class="a-without-styling color-grey">
                    <p>Phone: +44-333 014 4501 </p>
                  </a>
                  <a href="fax:443330144501" class="a-without-styling color-grey">
                    <p>Fax: +44-333 014 4502 </p>
                  </a>
                </div>
                <div class="footer opensans-400-16 color-grey">
                  <p style="margin-top:43px">Email: </p>
                  <a href="mailto:contact@manice.com" class="a-without-styling color-grey">
                    <p>contact@manice.com</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="tile right-side">
            <h2 class="lato-700-36">Contact Form</h2>




            <form class="contact-form" id="contact" action="contact.php" method="post">
              <div>
                <input type="text" placeholder="Your Name" name="name" class="contact-form-styling opensans-400-16" />
              </div>
              <div>
                <input type="email" placeholder="Your Email" name="email" class="contact-form-styling opensans-400-16 " />
              </div>
              <textarea name="message" placeholder="Your Message" cols="30" rows="8" class="contact-form-styling opensans-400-16"></textarea>
              <!-- <a name="submit" type="submit" class="btn btn-green btn-centered">Send</a> -->
              <button name="submit" type="submit" class="btn btn-green btn-centered" style="border: 1px solid rgb(255, 255, 255)">Siusti</button>
              <p class="opensans-400-italic-16 color-grey">
                We provide a professional service for private and commercial customers.
              </p>
            </form>
          </div>
        </div>
        <div class="customers-footer flex-container">
          <img src="images/logos/dummy-logo-1b.png" alt="logo" />
          <img src="images/logos/dummy-logo-2b.png" alt="logo" />
          <img src="images/logos/dummy-logo-3b.png" alt="logo" />
          <img src="images/logos/dummy-logo-4b.png" alt="logo" />
        </div>


      </div>
    </section>
    <?php include('views/footer_2.php'); ?>
    <script src="js.js"></script>
</body>

</html>