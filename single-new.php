<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Single New</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />

  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="single-news">
  <?php include('views/header.php'); ?>

  <main>
    <section class="section-heading flex-container">
      <h2 class="text">News</h2>
    </section>
    <section class="single-new">
      <div class="container">
        <div class="container-790">
          <h2 class="lato-700-36">Culpaqui officia deserunt mollit anim.</h2>
          <div class="flex-container news-icons color-grey">
            <div class="flex-container ">
              <i class="far fa-user"></i>
              <p>Admin</p>
            </div>
            <div class="flex-container">
              <i class="far fa-calendar"></i>
              <p>July 10, 2020</p>
            </div>
            <div class="flex-container">
              <i class="far fa-comment"></i>
              <p>No Comments</p>
            </div>
          </div>
        </div>
        <div class="wrapper">
          <img src="images/img/skyscaper_from_above2_large.jpg" alt="">
        </div>
        <div class="container-790 color-grey">
          <div class="flex-container large-letter-and-text">
            <span class="first-letter-large">R</span>
            <p class="next-to-letter-large">utenim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              utenim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
          </div>
          <div class="text">
            <div class="text-header">
              <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus
                in, viverra quis, feugiat a, tellus.
              </p>
              <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi
                vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus
                eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam
                nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
              </p>
              <p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam
                quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet
                nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
                quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>
            </div>
            <div class="list-spacing">
              <h4 class="lato-700-24">Ordered List</h4>
              <ol class="opensans-400-16">
                <li>Qui officia deserunt mollit anim id est laborum.
                </li>
                <li>Proident sunt in culpa qui officia.
                </li>
                <li>Officia deserunt mollit anim.</li>
              </ol>
            </div>
            <div class="list-spacing">
              <h4 class="lato-700-24">Unordered List</h4>
              <ul class="opensans-400-16">
                <li>Qui officia deserunt mollit anim id est laborum.
                </li>
                <li>Proident sunt in culpa qui officia.
                </li>
                <li>Officia deserunt mollit anim.</li>
              </ul>
            </div>
            <p>Tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam
              quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
            </p>
            <p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis
              ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.
              Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis
              gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>
          </div>
        </div>
      </div>
      </div>
      <div class="flex-container">
        <a href="news.php" class="btn btn-green btn-centered">Back</a>
      </div>
    </section>
    <?php include('views/footer.php'); ?>
    <script src="js.js"></script>
</body>

</html>