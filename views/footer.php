<section class="footer">
    <div class="full-background-color full-background-color-green">
        <div class="container flex-container">
            <div class="left-tile">
                <h3>Don't hesitate to contact us any time.</h3>
                <p>
                    Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur sint occaecat cupidatat.
                </p>
            </div>
            <div class="right-tile">
                <a href="contact.php" class="btn btn-white">Get a Quota</a>
            </div>
        </div>
    </div>
    <div class="full-background-color full-background-color-black">
        <div class="container flex-container footer-container">
            <div class="tile first-tile">
                <div class="first-tile-wrapper">
                    <img src="images/logos/logo-white.png" alt="company logo" />
                    <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur.
                    </p>
                    <div class="flex-container">
                        <i class="fab fa-facebook-f"></i>
                        <i class="fab fa-twitter"></i>
                        <i class="fab fa-instagram"></i>
                        <i class="fab fa-linkedin-in"></i>
                    </div>
                </div>
            </div>
            <div class="tile second-tile">
                <div class="second-tile-wrapper">
                    <h5>Our Company</h5>
                    <ul>
                        <li>
                            <a href="index.php">
                                >
                                <span>Homepage</span>
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                >
                                <span>About</span>
                            </a>
                        </li>
                        <li>
                            <a href="services.php">
                                >
                                <span>Services</span>
                            </a>
                        </li>
                        <li>
                            <a href="news.php">
                                >
                                <span>News</span>
                            </a>
                        </li>
                        <li>
                            <a href="contact.php">
                                >
                                <span>Contact</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tile third-tile">
                <div class="third-tile-wrapper">
                    <h5>Useful Links</h5>
                    <ul>
                        <li>
                            <a href="index.php">
                                >
                                <span>Homepage</span>
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                >
                                <span>About</span>
                            </a>
                        </li>
                        <li>
                            <a href="services.php">
                                >
                                <span>Services</span>
                            </a>
                        </li>
                        <li>
                            <a href="news.php">
                                >
                                <span>News</span>
                            </a>
                        </li>
                        <li>
                            <a href="contact.php">
                                >
                                <span>Contact</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tile forth-tile">
                <div class="forth-tile-wrapper">
                    <h5>Contact Info</h5>
                    <div class="flex-container">
                        <i class="fas fa-map-marker-alt"></i>
                        <p class="custom">
                            99 S.t Jomblo Park Pekanbaru 28292. Indonesia
                        </p>
                    </div>
                    <div class="flex-container">
                        <i class="fas fa-phone-alt"></i>
                        <a href="tel:0761654123987">
                            <p>(0761) 654-123987</p>
                        </a>
                    </div>
                    <div class="flex-container">
                        <i class="far fa-envelope"></i>
                        <a href="mailto:info@yoursite.com">
                            <p>info@yoursite.com</p>
                        </a>
                    </div>
                    <div class="flex-container">
                        <i class="far fa-clock"></i>
                        <p>Mon - Sat 09:00 - 17:00</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-background-color full-background-color-darkblack">
        <div class="flex-container">
            <p>© 2020 Manice Figma Template. All rights reserved</p>
        </div>
    </div>
</section>