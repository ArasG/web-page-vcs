<header>
    <div class="container flex-container">
        <a href="index.php">
            <img src="images/logos/logo.png" alt="company logo" />
        </a>

        <ul class="flex-container flex-container-nav main-nav">
            <li>
                <a href="index.php#about" class="about" id="nav-btn-about">About</a>
            </li>
            <li>
                <a href="index.php#services" class="services" id="nav-btn-services">Services</a>
            </li>
            <li>
                <a href="index.php#news" class="news" id="nav-btn-news">News</a>
            </li>
            <li><a href="contact.php" class="contact">Contact</a></li>
        </ul>
        <div class="mobile-nav">
            <a href="javascript:void(0);" onclick="showHideMobNav()">
                <i class="fas fa-bars"></i></a>

            <ul id="mMenu" class="flex-container">
                <li><a href="index.php#about" id="nav-btn-about-m">About</a></li>
                <li><a href="index.php#services" id="nav-btn-services-m">Services</a></li>
                <li><a href="index.php#news" id="nav-btn-news-m">News</a></li>
                <li><a href="contact.php" id="nav-btn-contact-m">Contact</a></li>
            </ul>
        </div>
    </div>
</header>