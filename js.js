// ///////////////..... FANCYAPP CAROUSEL...../////////////
const cardSlider = new Carousel(document.querySelector("#cardSlider"), {
  Dots: true,
  slidesPerPage: "auto",
  friction: 0.95,
  infinite: false,
});

// ///////////////..... MOBILE NAV...../////////////
function showHideMobNav() {
  console.log("showHideMobNav");
  let x = document.getElementById("mMenu");
  if (x.style.display === "block") {
    x.style.display = "none";
    console.log("1");
  } else {
    x.style.display = "block";
    console.log("2");
  }
}

function hideMobNav() {
  let x = document.getElementById("mMenu");
  x.style.display = "none";
  console.log("xxxxx");
}

const btnAboutM = document.querySelector("#nav-btn-about-m");
const btnServicesM = document.querySelector("#nav-btn-services-m");
const btnNewsM = document.querySelector("#nav-btn-news-m");

btnAboutM.addEventListener("click", function () {
  hideMobNav();
  console.log("btnAboutM");
});

btnServicesM.addEventListener("click", function () {
  hideMobNav();
});

btnNewsM.addEventListener("click", function () {
  hideMobNav();
});

// ///////////////..... PAGE SCROLL...../////////////
const btnAbout = document.querySelector("#nav-btn-about");
const btnServices = document.querySelector("#nav-btn-services");
const btnNews = document.querySelector("#nav-btn-news");
const sectionAbout = document.querySelector("#about");
const sectionServices = document.querySelector("#services");
const sectionNews = document.querySelector("#news");

function extractUrlLastPart(string) {
  return string.substring(string.lastIndexOf("/") + 1, string.length);
}

btnAbout.addEventListener("click", function (e) {
  console.log("btnAbout");
  if (
    extractUrlLastPart(window.location.href) === "index.php#about" ||
    extractUrlLastPart(window.location.href) === "index.php"
  )
    e.preventDefault();
  sectionAbout.scrollIntoView({ behavior: "smooth", block: "center" });
});

btnServices.addEventListener("click", function (e) {
  if (
    extractUrlLastPart(window.location.href) === "index.php#services" ||
    extractUrlLastPart(window.location.href) === "index.php"
  )
    e.preventDefault();
  sectionServices.scrollIntoView({ behavior: "smooth", block: "center" });
  console.log(window.location.href);
});

btnNews.addEventListener("click", function (e) {
  if (
    extractUrlLastPart(window.location.href) === "index.php#news" ||
    extractUrlLastPart(window.location.href) === "index.php"
  )
    e.preventDefault();
  sectionNews.scrollIntoView({ behavior: "smooth", block: "center" });
});

// ///////////////..... Reveal Sections...../////////////
const allSections = document.querySelectorAll(".section-h");
const revealSection = function (entries, observer) {
  const [entry] = entries;
  // console.log(entry)
  if (!entry.isIntersecting) return;
  entry.target.classList.remove("section--hidden");
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.1,
});

allSections.forEach(function (section) {
  setTimeout(function () {
    // console.log(window.pageYOffset);
    if (window.pageYOffset <= 100) {
      sectionObserver.observe(section);
      section.classList.add("section--hidden");
    }
  }, 20);
});

// ////////..... count animation...../////////
function animateValue(id, start, duration) {
  var obj = document.getElementById(id);
  var end = obj.textContent;
  if (start === end) return;
  var range = end - start;
  var current = start;
  var increment = end > start ? 1 : -1;
  var stepTime = Math.abs(Math.floor(duration / range));
  var timer = setInterval(function () {
    current += increment;
    obj.innerHTML = current;
    if (current == end) {
      clearInterval(timer);
    }
  }, stepTime);
}

function logHeight() {
  const offset = window.pageYOffset;
  const height = window.innerHeight;
  if (offset > 1850) window.removeEventListener("scroll", logHeight);
  if (height + offset >= 1650 && height + offset < 1850) {
    animateValue("customers", 0, 3000);
    animateValue("consuldents", 0, 3000);
    animateValue("years", 0, 3000);
    animateValue("finance", 0, 3000);
    window.removeEventListener("scroll", logHeight);
  }
}

window.addEventListener("scroll", logHeight);
