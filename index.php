<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Index</title>
  <link rel="shortcut icon" href="images/logos/logo-tab.png">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&family=Open+Sans:wght@300;400&family=Poppins&family=Roboto:ital,wght@0,300;0,400;0,700;1,100;1,300;1,700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <script src="https://kit.fontawesome.com/e207e0e544.js" crossorigin="anonymous"></script>
</head>

<body class="index">

  <?php include('views/header.php'); ?>
  <main class="index">
    <section class="hero">
      <div class="container-background"></div>
      <div class="container">
        <div class="hero-header">
          <h1>We are driven to create a better financial future</h1>
          <p>
            Our proffesional team works to increase productivity and cost
            effectiveness on the market.
          </p>
        </div>
        <div class="flex-container flex-container-hero-buttons">
          <a href="about.php" class="btn btn-black">Learn More</a>
          <a href="contact.php" class="btn btn-green">Contact Us</a>
        </div>
        <div class="hero-footer flex-container">
          <div class="left-tile">
            <h3>Business Solutions</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
              quasi illo fugit! Asperiores impedit numquam repellat blanditiis
              unde reprehenderit hic repellendus, tenetur voluptas quasi
              aperiam ratione corrupti ea veniam non?
            </p>
          </div>
          <div class="right-tile">
            <img src="images/img/hi_fifve.jpg" alt="peoples in the office" class="responsive" />
          </div>
        </div>
      </div>
    </section>
    <section class="about section-h" id="about">
      <div class="container">
        <div class="header-two-tiles flex-container">
          <h2>Experianced in mortgage and financial advice!</h2>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam
            accusamus inventore vero obcaecati, magni cupiditate! Quia aut
            praesentium debitis veritatis quas, ratione magni harum ab
            maiores, corrupti illo ipsa ex rem deleniti corporis odio commodi
            quae.
          </p>
        </div>
        <div class="header-number-tiles flex-container">
          <div class="tile">
            <p class="number" id="customers">734</p>
            <p class="text">Satisfied Customers</p>
          </div>
          <div class="tile">
            <p class="number" id="consuldents">542</p>
            <p class="text">Consulteds</p>
          </div>
          <div class="tile">
            <p class="number" id="years">17</p>
            <p class="text">Years Experience</p>
          </div>
          <div class="tile">
            <p class="number" id="finance">324</p>
            <p class="text">Finance Help</p>
          </div>
        </div>
      </div>
      <!-- WIP -->
      <div class="container-background">
        <div class="about-background">
          <div class="company-value-wrapper flex-container">
            <div class="company-value-tile company-value-left-tile">
              <div class="tile-wrapper">
                <div class="about-text">
                  <div class="flex-container two-color-h3">
                    <h3>Company</h3>
                    <h3>Value</h3>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Nisi voluptatum voluptas fugiat perspiciatis, laudantium
                    saepe modi. Aspernatur nam deserunt fugiat optio!
                    Recusandae, aut nesciunt architecto iusto doloremque
                    dolorem fuga ullam.
                  </p>
                  <div class="about-text-left flex-container">
                    <i class="fas fa-check"></i>
                    <p>
                      Ipsum dolor sit amet consectetur adipisicing elit. Sint
                      officiis dolor nisi! Eum quis dolorem
                    </p>
                  </div>

                  <div class="about-text-left flex-container">
                    <i class="fas fa-check"></i>
                    <p>
                      Dolor sit, amet consectetur adipisicing elit. Nihil!
                    </p>
                  </div>
                  <div class="about-text-left flex-container">
                    <i class="fas fa-check"></i>
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Eum quis dolorem praesentium cumque.
                    </p>
                  </div>
                  <a href="about.php" class="btn btn-green">About Company</a>
                </div>
              </div>
            </div>
            <div class="company-value-tile company-value-right-tile">
              <div class="green-box"></div>
              <img src="images/img/man_writes_on_board.jpg" alt="man writes on whiteboard" />
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="services section-h" id="services">
      <div class="container">
        <div class="flex-container two-color-h3">
          <h3>Our</h3>
          <h3>Services</h3>
        </div>
        <div class="services-tiles flex-container">
          <div class="services-tile flex-container">
            <img src="images/icons/arrow.png" alt="arrow icon" />
            <div class="text">
              <h4>Financial Analysis</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
          <div class="services-tile flex-container">
            <img src="images/icons/breafcase.png" alt="breafcase icon" />
            <div class="text">
              <h4>Business Solutions</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
          <div class="services-tile flex-container">
            <img src="images/icons/people.png" alt="people icon" />

            <div class="text">
              <h4>Client Management</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
          <div class="services-tile flex-container">
            <img src="images/icons/bubbles.png" alt="bubbles icon" />
            <div class="text">
              <h4>Online Conslting</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
          <div class="services-tile flex-container">
            <img src="images/icons/pie.png" alt="pie icon" />
            <div class="text">
              <h4>Business Opurtunities</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
          <div class="services-tile flex-container">
            <img src="images/icons/monitor.png" alt="monitor icon" />
            <div class="text">
              <h4>IT Consulting</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Impedit minus nostrum.
              </p>
            </div>
          </div>
        </div>
        <div class="flex-container">
          <a href="services.php" class="btn btn-green btn-centered">Learn More</a>
        </div>
      </div>
    </section>
    <section class="customers section-h">
      <div class="container-background">
        <div class="full-background-color"></div>
        <div class="container">
          <div class="section-header">
            <div>
              <h2>All of our customers trust their success to us</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nemo
                maiores facilis commodi eaque exercitationem veritatis fugit a
                aliquam, sint alias quisquam magni. Repudiandae ex nobis iure
                nulla expedita! Itaque nobis quis quod.
              </p>
            </div>
          </div>
          <div class="customer-image">
            <div class="customer-background">
              <div class="text">
                <p class="roboto-italic-27">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Voluptas voluptatem sequi recusandae unde quae veritatis
                  sint adipisci illo earum deserunt, iste excepturi placeat
                  cupiditate natus exercitationem architecto ipsa.
                </p>
                <p>Rudhi Sasmito</p>
                <p>- Head Of Operations</p>
              </div>
            </div>
          </div>
          <div class="customers-footer flex-container">
            <img src="images/logos/dummy-logo-1b.png" alt="logo" />
            <img src="images/logos/dummy-logo-2b.png" alt="logo" />
            <img src="images/logos/dummy-logo-3b.png" alt="logo" />
            <img src="images/logos/dummy-logo-4b.png" alt="logo" />
          </div>
        </div>
      </div>
    </section>
    <!-- //////////////////.....NEWS.....///////////////// -->
    <section class="news section-h" id="news">
      <div class="container">
        <div class="flex-container two-color-h3">
          <h3>Recent</h3>
          <h3>News</h3>
        </div>
        <div id="cardSlider" class="carousel mb-6 max-w-6xl mx-auto bg-gray-50 py-10">
          <div class="carousel__viewport px-12">
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/skyscaper_from_above.jpg" alt="foto of buldings" />
                  <h5>Reprehenderit in voluptate velit esse cillum</h5>
                  <p class="opensans-400-16 color-grey">
                    July 29, 2020 | Arts
                  </p>
                  <p class="tile-body opensans-400-16 color-grey">
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident sunt in culpa.
                  </p>
                  <a href="single-new.php" class="tile-footer">Read More</a>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/office.jpg" alt="foto of buldings" />
                </div>
                <h5>
                  Caecat cupidatat non proident sunt in culpa qui officia
                </h5>
                <p class="opensans-400-16 color-grey">July 29, 2020 | Arts</p>
                <p class="tile-body opensans-400-16 color-grey">
                  Duis aute irure dolor in reprehenderit in voluptate velit
                  esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident sunt in culpa.
                </p>
                <a href="single-new.php" class="tile-footer">Read More</a>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/typing_laptop.jpg" alt="foto of buldings" />
                  <h5>Quis nostrud exercitation ullamco laboris nisi</h5>
                  <p class="opensans-400-16 color-grey">
                    July 29, 2020 | Arts
                  </p>
                  <p class="tile-body opensans-400-16 color-grey">
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident sunt in culpa.
                  </p>
                  <a href="single-new.php" class="tile-footer">Read More</a>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/money.jpg" alt="foto of buldings" />
                  <h5>Reprehenderit in voluptate velit esse cillum</h5>
                  <p class="opensans-400-16 color-grey">
                    July 29, 2020 | Arts
                  </p>
                  <p class="tile-body opensans-400-16 color-grey">
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident sunt in culpa.
                  </p>
                  <a href="single-new.php" class="tile-footer">Read More</a>
                </div>
              </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/skyscaper_from_above2.jpg" alt="foto of buldings" " />
                </div>
                <h5>
                  Caecat cupidatat non proident sunt in culpa qui officia
                </h5>
                <p class=" opensans-400-16 color-grey">July 29, 2020 | Arts</p>
                  <p class="tile-body opensans-400-16 color-grey">
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident sunt in culpa.
                  </p>
                  <a href="single-new.php" class="tile-footer">Read More</a>
                </div>
            </figure>
            <figure class="carousel__slide py-0 px-4 w-1/3">
              <div class="tile-of-news">
                <div class="wrapper">
                  <img src="images/img/money_in_mug.jpg" alt="foto of buldings" />
                  <h5>Quis nostrud exercitation ullamco laboris nisi</h5>
                  <p class="opensans-400-16 color-grey">
                    July 29, 2020 | Arts
                  </p>
                  <p class="tile-body opensans-400-16 color-grey">
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident sunt in culpa.
                  </p>
                  <a href="single-new.php" class="tile-footer">Read More</a>
                </div>
              </div>
            </figure>
          </div>
        </div>
        <div class="flex-container">
          <a href="news.php" class="btn btn-green btn-centered">More News</a>
        </div>
      </div>
    </section>
    <!-- ////////////////.....FOOTER...../////////////// -->
    <?php include('views/footer.php'); ?>
  </main>
  <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
  <script src="js.js"></script>
</body>

</html>